$('.city_seacrh input').on("change input", function () {
  if ($(this).val().length > 0) {
    $(this).parents('.city_seacrh').find('.city_seacrh_result_block').addClass('active');
  } else {
    $(this).parents('.city_seacrh').find('.city_seacrh_result_block').removeClass('active');
  }
});

const swiper__new = new Swiper('.swiper_new_product', {
  // Optional parameters
  direction: 'horizontal',
  loop: false,
  slidesPerView: 5,
  spaceBetween: 30,
  pagination: {
    el: '.swiper-pagination_new',
  },
  breakpoints: {
    // when window width is >= 320px
    1: {
      slidesPerView: 1,
      spaceBetween: 20
    },
    350: {
      slidesPerView: 1.2,
      spaceBetween: 30
    },
    // when window width is >= 480px
    425: {
      slidesPerView: 1,
      spaceBetween: 15
    },
    // when window width is >= 640px
    500: {
      slidesPerView: 2,
      spaceBetween: 20
    },
    720: {
      slidesPerView: 3,
      spaceBetween: 20
    },
    992: {
      slidesPerView: 4,
      spaceBetween: 30,
      pagination: {
        el: '.swiper-pagination_new',
      },
    },
    1025: {
      slidesPerView: 5,
      spaceBetween: 30
    }
  },
  // Navigation arrows
  navigation: {
    nextEl: '.swiper_new_btn_next',
    prevEl: '.swiper_new_btn_prev',
  },

});

const swiper_top = new Swiper('.swiper_top_product', {
  // Optional parameters
  direction: 'horizontal',
  loop: false,
  spaceBetween: 30,

  pagination: {
    el: '.swiper-pagination_top',
  },
  breakpoints: {
    // when window width is >= 320px
    1: {
      slidesPerView: 'auto',
      spaceBetween: 10
    },
    992: {
      slidesPerView: 'auto',
      spaceBetween: 32
    },
  },

  // Navigation arrows
  navigation: {
    nextEl: '.swiper_top_btn_next',
    prevEl: '.swiper_top_btn_prev',
  },

});

const swiper_hot_deals = new Swiper('.swiper_hot_deals', {
  // Optional parameters
  direction: 'horizontal',
  slidesPerView: 3,
  spaceBetween: 30,

  pagination: {
    el: '.swiper-pagination_hot',
  },
  loop: false,
  breakpoints: {
    // when window width is >= 320px
    1: {
      slidesPerView: 1.2,
      spaceBetween: 20,
    },
    // when window width is >= 480px
    425: {
      slidesPerView: 1.3,
      spaceBetween: 30
    },
    // when window width is >= 640px
    640: {
      slidesPerView: 2,
      spaceBetween: 20
    },
    992: {
      slidesPerView: 3,
      spaceBetween: 30,

      pagination: {
        el: '.swiper-pagination_hot',
      },
    },
    1025: {
      slidesPerView: 3,
      spaceBetween: 30
    }
  },

  // Navigation arrows
  navigation: {
    nextEl: '.swiper_hot_next',
    prevEl: '.swiper_hot_prev',
  },
});

const swiper_popular_brand = new Swiper('.swiper_popular_brand', {
  // Optional parameters
  direction: 'horizontal',
  loop: false,
  spaceBetween: 30,
  freeMode: false,

  pagination: {
    el: '.swiper-pagination_top',
  },
  breakpoints: {
    // when window width is >= 320px
    1: {
      slidesPerView: 2,
      spaceBetween: 9,
    },
    650: {
      slidesPerView: 'auto',
      spaceBetween: 9,
    },
    1200: {
      slidesPerView: 6,
      spaceBetween: 30,
    },
  },

});


$('.mobile_search_icon').on('click', function () {
  $('.seacrh_mobile_block').addClass('active')
})

$('#mobile_search').on('change input', function () {
  if ($(this).val().length > 0) {
    $('.search_mob_res').addClass('active')
  } else {
    $('.search_mob_res').removeClass('active')
  }
})

$(document).mouseup(function (e) {
  var container = $(".seacrh_mobile_block");
  if (container.has(e.target).length === 0) {
    container.removeClass('active');
  }
});


const swiper_per__hours = new Swiper('.swiper_per_hours', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 1.2,
  spaceBetween: 10,
  breakpoints: {
    768: {
      slidesPerView: 2,
      spaceBetween: 30
    },
  },

});

var swiper_reviews = new Swiper('.swiper_reviews', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 2,
  spaceBetween: 30,
  navigation: {
    nextEl: '.swiper_review_btn_next',
    prevEl: '.swiper_review_btn_prev',
  },
  breakpoints: {
    1: {
      slidesPerView: 1,
      spaceBetween: 30,
      pagination: {
        el: '.swiper-pagination',
      },
    },
    650: {
      slidesPerView: 'auto',
      spaceBetween: 30,
      pagination: {
        el: '.swiper-pagination',
      },
    },
    1221: {
      slidesPerView: 2,
      spaceBetween: 30
    },
  },
});

const swiper_service = new Swiper('.swiper_service', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 1.5,
  spaceBetween: 10,
  breakpoints: {
    600: {
      slidesPerView: 3.5,
      spaceBetween: 30
    },
    850: {
      slidesPerView: 4.5,
      spaceBetween: 30
    },
    1025: {
      slidesPerView: 6,
      spaceBetween: 30
    },
  },
});

var swiper_time = new Swiper('.swiper_time', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 'auto',
  spaceBetween: 8,
  freeMode: true,
  navigation: {
    nextEl: '.swiper_time_btn_next',
    prevEl: '.swiper_time_btn_prev',
  },
});

var swiper__hostel = new Swiper('.swiper__hostel', {
  direction: 'horizontal',
  loop: false,
  autoplay: {
    delay: 4000
  },
  slidesPerView: 2.3,
  spaceBetween: 8,
  navigation: {
    nextEl: '.swiper_hostel_btn_next',
    prevEl: '.swiper_hostel_btn_prev',
  },
});

var swiper_specialists = new Swiper('.swiper_specialists', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 2,
  spaceBetween: 30,
  navigation: {
    nextEl: '.swiper_specialists_btn_next',
    prevEl: '.swiper_specialists_btn_prev',
  },
  breakpoints: {
    1: {
      slidesPerView: 1,
      spaceBetween: 15,
      pagination: {
        el: '.swiper-pagination',
      },
    },
    993: {
      slidesPerView: 2,
      spaceBetween: 30
    },
  },
});

function slideNet() {
  if ($(window).width() < 992) {
    let itemsSlide = $('.swiper__index_catalog .swiper-wrapper .myitem').toArray();
    let half_length = Math.ceil(itemsSlide.length / 2)
    let firstHalf = itemsSlide.slice(0, half_length)
    let secondHalf = itemsSlide.slice(half_length)
    $.each(firstHalf, function () {
      $(this).appendTo($('.swiper__index_catalog .mobile_left'));
    });
    $.each(secondHalf, function () {
      $(this).appendTo($('.swiper__index_catalog .mobile_right'));
    });
  } else if ($(window).width() > 991) {
    $('.swiper__index_catalog .swiper-slide').children().appendTo($('.swiper__index_catalog .swiper-wrapper'))
  }
}

slideNet()

var swiper__index_catalog = undefined;

function initSwiper() {
  var screenWidth = $(window).width();
  if (screenWidth < 992 && swiper__index_catalog == undefined) {
    swiper__index_catalog = new Swiper('.swiper__index_catalog', {
      spaceBetween: 30,
      slidesPerView: 4,
      breakpoints: {
        1: {
          slidesPerView: 1,
          spaceBetween: 10,
        },
        450: {
          slidesPerView: 1,
          spaceBetween: 15,
        },
        1025: {
          slidesPerView: 6,
          spaceBetween: 30
        },
      },
    });
  } else if (screenWidth > 991 && swiper__index_catalog != undefined) {
    swiper__index_catalog.destroy();
    swiper__index_catalog = undefined;
    jQuery('.swiper-wrapper').removeAttr('style');
    jQuery('.swiper-slide').removeAttr('style');
  }
}

//Swiper plugin initialization
initSwiper();

//Swiper plugin initialization on window resize
$(window).on('resize', function () {
  initSwiper();
  slideNet()
  orderResize()
});


$('.fly__menu__item').click(function () {
  $(this).parent('.fly__menu__wrapper').find('.active').removeClass('active')
  $(this).find('.icon').addClass('active')
});

$('.nav__mobile__block').click(function () {
  $(this).toggleClass('active')
});
if (screen.width < 450) {
  $('.footer__item').click(function () {
    $(this).find('.wrapper').slideToggle(500);
    $(this).find('.icon').toggleClass('active');
    $(this).find('.wrapper').toggleClass('active')
  });
}

var swiper__top_banner = new Swiper('.swiper_top_banner', {
  // Optional parameters
  direction: 'horizontal',
  loop: false,
  slidesPerView: 1,
  spaceBetween: 30,
  pagination: {
    el: '.swiper-pagination_banner',
  },
});

$('.nav__mobile__block').click(function () {
  $('.mobile__nav__block').toggleClass('active')
  $('body').toggleClass('lock')
  $('html').toggleClass('lock')
});

$('.size__item').click(function () {
  $(this).parent('.size__block').find('.active').removeClass('active')
  $(this).addClass('active')
});

$('.color__item').click(function () {
  $(this).parent('.color__block').find('.active').removeClass('active')
  $(this).addClass('active')
});

$('.card__banner .close').click(function () {
  $('.card__banner').addClass('active')
});

$('.btn__descr__item').click(function () {
  $(this).parent('.block__btn__descr').find('.active').removeClass('active')
  $(this).addClass('active')
});

$('.filter__item').click(function () {
  $('.filter__item').removeClass('active')
  $(this).addClass('active')
});

$('.slide__item, .sale_slide_btn .title').click(function () {
  $(this).parent('.sale_slide_btn').find('.slide__item').toggleClass('active')
});

$('.filter__name').click(function () {
  $(this).next('.content').slideToggle(300);
  $(this).toggleClass('active')
});

$('.number__item').click(function () {
  $(this).parent('.number__pagintaion').find('.active').removeClass('active')
  $(this).addClass('active')
});

$('.per__page__item').click(function () {
  $(this).parent('.per__page__block').find('.active').removeClass('active')
  $(this).addClass('active')
});

$('.express_btn .adres').click(function () {
  $('.popup_adres').addClass('active')
});

$('.nav .lk__block .lk__btn').click(function () {
  $('.popup__reg').addClass('active')
});

$('.popup__reg .close').click(function () {
  $('.popup__reg').removeClass('active')
});

$('.popup__reg .get__code').click(function () {
  $('.popup__reg').removeClass('active')
  $('.popup_code').addClass('active')
});

$('.popup_code .close').click(function () {
  $('.popup_code').removeClass('active')
});

$('.popup_code .back__popup').click(function () {
  $('.popup_code').removeClass('active')
  $('.popup__reg').addClass('active')
});

$('.delivery__item').click(function () {
  $(this).parent('.deliver__wrapper').find('.active').removeClass('active')
  $(this).addClass('active')
});


$('.delivery__item.express').click(function () {
  $(this).parents('.order__block').find('.delivery__express.active').removeClass('active')
  $(this).parents('.order__block').find('.delivery__express.express').addClass('active')
  $(this).parents('.order__block').find('.express__time').addClass('active')
  $(this).parents('.order__block').find('.pickup__block').removeClass('active')
});

$('.delivery__item.courier').click(function () {
  $(this).parents('.order__block').find('.delivery__express.active').removeClass('active')
  $(this).parents('.order__block').find('.express__time.active').removeClass('active')
  $(this).parents('.order__block').find('.delivery__express.courier').addClass('active')
  $(this).parents('.order__block').find('.pickup__block').removeClass('active')
});

$('.delivery__item.pickup').click(function () {
  $(this).parents('.order__block').find('.delivery__express.active').removeClass('active')
  $(this).parents('.order__block').find('.delivery__express.courier.active').removeClass('active')
  $(this).parents('.order__block').find('.express__time.active').removeClass('active')
  $(this).parents('.order__block').find('.pickup__block').addClass('active')
});

$('.btn__item.map').click(function () {
  $(this).parents('.pickup__btn__block').find('.active').removeClass('active')
  $(this).addClass('active')
  $(this).parents('.pickup__btn__block').find('span.active').removeClass('active')
});

$('.btn__item.list').click(function () {
  $(this).parents('.pickup__btn__block').find('.active').removeClass('active')
  $(this).addClass('active')
  $(this).parents('.pickup__btn__block').find('span').addClass('active')
});

$('.btn__item.cat').click(function () {
  $(this).parents('.pickup__btn__block').find('.active').removeClass('active')
  $(this).addClass('active')
  $(this).parents('.pickup__btn__block').find('span').addClass('active')
  $(this).parents('.birthday__content').find('.dog__block.active').removeClass('active')
  $(this).parents('.birthday__content').find('.cat__block').addClass('active')
});

$('.btn__item.dog').click(function () {
  $(this).parents('.pickup__btn__block').find('.active').removeClass('active')
  $(this).addClass('active')
  $(this).parents('.pickup__btn__block').find('span.active').removeClass('active')
  $(this).parents('.birthday__content').find('.cat__block.active').removeClass('active')
  $(this).parents('.birthday__content').find('.dog__block').addClass('active')
});

$('.btn__item.adres_courier').click(function () {
  $(this).parents('.pickup__btn__block').find('.active').removeClass('active')
  $(this).addClass('active')
  $(this).parents('.adres_delivery').find('span').addClass('active')
  $(this).parents('.adres_delivery').find('.pickup.active').removeClass('active')
  $(this).parents('.adres_delivery').find('.courier').addClass('active')
});

$('.btn__item.adres_pickup').click(function () {
  $(this).parents('.pickup__btn__block').find('.active').removeClass('active')
  $(this).addClass('active')
  $(this).parents('.adres_delivery').find('span.active').removeClass('active')
  $(this).parents('.adres_delivery').find('.courier.active').removeClass('active')
  $(this).parents('.adres_delivery').find('.pickup').addClass('active')
});


$('.btn__item.map').click(function () {
  $('.list__block').removeClass('active')
  $('.map__block').addClass('active')
});

$('.btn__item.list').click(function () {
  $('.map__block').removeClass('active')
  $('.list__block').addClass('active')
});

$('.cash__btn').click(function () {
  $(this).parents('.cash__btn__block').find('.active').removeClass('active')
  $(this).addClass('active')
});

$('.payment__block .delivery__item').click(function () {
  $(this).parents('.payment__block').find('.cash__block.active').removeClass('active')
});

$('.payment__block .delivery__item.cash').click(function () {
  $(this).parents('.payment__block').find('.cash__block').addClass('active')
});

$('.faq__item').click(function () {
  $(this).find('.icon').toggleClass('active')
  $(this).find('.text').slideToggle(300);
});

$('.adres_delivery .wrapper .add_adres').click(function () {
  $(this).parents('body').find('.popup__adres_del').addClass('active')
});

$('.adres_delivery .wrapper .icon__block .icon').click(function () {
  $(this).parents('body').find('.popup__adres_del').addClass('active')
});

$('.mypet .add_pet').click(function () {
  $(this).parents('body').find('.popup_pet').addClass('active')
});

$('.popup .close').click(function () {
  $(this).parents('body').find('.popup.active').removeClass('active')
});

$('.myData .content .icon.name').click(function () {
  $(this).parents('body').find('.popup__name').addClass('active')
});

$('.myData .content .icon.number').click(function () {
  $(this).parents('body').find('.popup__number').addClass('active')
});

$('.popup .edit').click(function () {
  $(this).parents('body').find('.popup').removeClass('active')
});

$(document).ready(function () {
  $('.js-example-basic-single').select2();
});

$('.how__order .btn__item').click(function () {
  $(this).parents('.pickup__btn__block').find('.btn__item.active').removeClass('active')
  $(this).addClass('active')
});

$('.how__order .btn__item.pasport').click(function () {
  $(this).parents('.pickup__btn__block').find('span.point1').removeClass('point1')
  $(this).parents('.pickup__btn__block').find('span.active').removeClass('active')
});

$('.how__order .btn__item.catalog').click(function () {
  $(this).parents('.pickup__btn__block').find('span').addClass('point1')
  $(this).parents('.pickup__btn__block').find('span.active').removeClass('active')
});

$('.how__order .btn__item.express-cat').click(function () {
  $(this).parents('.pickup__btn__block').find('span').addClass('active')
  $(this).parents('.pickup__btn__block').find('span.point1').removeClass('point1')
});


var swiper_image_block = new Swiper('.swiper_image_block', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 'auto',
  spaceBetween: 24,
  freeMode: true,

});

var swiper_birthday = new Swiper('.swiper_birthday', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 'auto',
  spaceBetween: 30,
  freeMode: true,
  breakpoints: {
    // when window width is >= 320px
    1: {
      slidesPerView: 'auto',
      spaceBetween: 20
    },
    992: {
      slidesPerView: 'auto',
      spaceBetween: 30
    },
  },
});

$('.select').on('click', function () {
  $(this).toggleClass('open')
})

$('.select.city .option_list .option').on('click', function () {
  var getZag = document.querySelector('.select_name.city');
  var getLabel = $(this).text();
  getZag.innerText = getLabel;
});

$('.select.pet .option_list .option').on('click', function () {
  var getZag = document.querySelector('.select_name.pet');
  var getLabel = $(this).text();
  getZag.innerText = getLabel;
});

$('.select.vid .option_list .option').on('click', function () {
  var getZag = document.querySelector('.select_name.vid');
  var getLabel = $(this).text();
  getZag.innerText = getLabel;
});

$('.select.poroda .option_list .option').on('click', function () {
  var getZag = document.querySelector('.select_name.poroda');
  var getLabel = $(this).text();
  getZag.innerText = getLabel;
});

$('.select.years .option_list .option').on('click', function () {
  var getZag = document.querySelector('.select_name.years');
  var getLabel = $(this).text();
  getZag.innerText = getLabel;
});

$(document).mouseup(function (e) {
  var container = $(".select");
  if (container.has(e.target).length === 0) {
    container.removeClass('open');
  }
});

$('.how__order .btn__item.catalog').on('click', function () {
  $('.how__order .wrapper').removeClass('active')
  $('.how__order .wrapper.main').addClass('active')
})

$('.how__order .btn__item.express-cat').on('click', function () {
  $('.how__order .wrapper').removeClass('active')
  $('.how__order .wrapper.exspress').addClass('active')
})

$('.how__order .btn__item.pasport').on('click', function () {
  $('.how__order .wrapper').removeClass('active')
  $('.how__order .wrapper.vet').addClass('active')
})

var swiper_pickup = new Swiper('.swiper_pickup', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 'auto',
  spaceBetween: 0,
  freeMode: true,
});


$('.swiper_pickup .swiper-slide .btn__item').on('click', function () {
  $('.swiper_pickup .swiper-slide .btn__item').removeClass('active')
  $(this).addClass('active')
})

var swiper_filter_head = new Swiper('.swiper_filter_head', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 'auto',
  spaceBetween: 16,
  freeMode: true,
});

var swiper_srav = new Swiper('.swiper_srav', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 4,
  spaceBetween: 32,
  freeMode: false,
  navigation: {
    nextEl: '.btn_srav_next',
    prevEl: '.btn_srav_prev',
  },
  allowTouchMove: false,
  breakpoints: {
    // when window width is >= 320px
    1: {
      slidesPerView: 2,
      spaceBetween: 10
    },
    650: {
      slidesPerView: 2,
      spaceBetween: 10
    },
    900: {
      slidesPerView: 3,
      spaceBetween: 20
    },
    1200: {
      slidesPerView: 4,
      spaceBetween: 32
    },
  },
});

var comprasion_swiper = new Swiper('.comprasion_swiper', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 4,
  spaceBetween: 32,
  freeMode: false,
  allowTouchMove: false,
  navigation: {
    nextEl: '.btn_srav_next',
    prevEl: '.btn_srav_prev',
  },
  breakpoints: {
    1: {
      slidesPerView: 2,
      spaceBetween: 10
    },
    650: {
      slidesPerView: 2,
      spaceBetween: 10
    },
    900: {
      slidesPerView: 3,
      spaceBetween: 20
    },
    1200: {
      slidesPerView: 4,
      spaceBetween: 32
    },
  },
});

var swiper_descr_srav = new Swiper('.swiper_descr_srav', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 'auto',
  spaceBetween: 0,
  freeMode: false,
});

$('.popup_adres .close').on('click', function () {
  $('.popup_adres').removeClass('active')
})

// Счетчик на странице карточки товара
function getInpValue() {
  let inpCount = $('.value__block .number').val();
  $('.value__block .plus').on('click', function () {
    if (inpCount < 99) {
      $('.value__block .number').val(++inpCount)
    }
  })
  $('.value__block .minus').on('click', function () {
    if (inpCount > 1) {
      $('.value__block .number').val(--inpCount)
    } else (
      $('.value__block .number').val()
    )
  })
}

setInterval(getInpValue, 1)

$('.select_block.weight').on('click', function () {
  $(this).toggleClass('open')
});

$('.select_block.weight .option_item').on('click', function () {
  var weight = document.querySelector('.select_block.weight .select_title')
  var option = $(this).text()
  weight.innerHTML = option;
  console.log(weight)
  console.log(option)
});

$('.card__descr__block .btn__item.descr').on('click', function () {
  $('.card__descr__block .position__block .wrapper').removeClass('active')
  $('.card__descr__block .position__block .wrapper.descr').addClass('active')
})

$('.card__descr__block .btn__item.har').on('click', function () {
  $('.card__descr__block .position__block .wrapper').removeClass('active')
  $('.card__descr__block .position__block .wrapper.har').addClass('active')
})

$('.card__descr__block .btn__item.nal').on('click', function () {
  $('.card__descr__block .position__block .wrapper').removeClass('active')
  $('.card__descr__block .position__block .wrapper.nal').addClass('active')
})

var swiper_card_thumb = new Swiper('.swiper_card_thumb', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 'auto',
  spaceBetween: 20,
  freeMode: true,
  watchSlidesProgress: true,
});

var swiper_card_big = new Swiper('.swiper_card_big', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 1,
  spaceBetween: 0,
  freeMode: false,
  effect: 'fade',
  fadeEffect: {
    crossFade: true
  },
  thumbs: {
    swiper: swiper_card_thumb,
  },
  breakpoints: {
    // when window width is >= 320px
    1: {
      slidesPerView: 1,
      spaceBetween: 0,
      pagination: {
        el: '.swiper-pagination',
      },
    },
    650: {
      slidesPerView: 1,
      spaceBetween: 0
    },
  },
});

$(document).mouseup(function (e) {
  var container = $(".popup_rewiews");
  if (container.has(e.target).length === 0) {
    container.removeClass('active');
  }
});

$(document).ready(function () {

  $("input[type='radio']").click(function () {
    var sim = $("input[type='radio']:checked").val();
    //alert(sim);
    if (sim < 3) {
      $('.myratings').css('color', '#231F21');
      $(".myratings").text(sim);
    } else {
      $('.myratings').css('color', '#231F21');
      $(".myratings").text(sim);
    }

    $('.star_value .text').text('Ваше оценка:')
    $('.star_value .text').css('color', '#231F21')

  });


});

function sortListDir() {
  var list, i, switching, b, shouldSwitch, dir, switchcount = 0;
  list = document.getElementById("id01");
  switching = true;
  dir = "asc";
  while (switching) {
    switching = false;
    b = list.getElementsByTagName("LI");
    for (i = 0; i < (b.length - 1); i++) {
      shouldSwitch = false;
      if (dir == "asc") {
        if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (b[i].innerHTML.toLowerCase() < b[i + 1].innerHTML.toLowerCase()) {
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      b[i].parentNode.insertBefore(b[i + 1], b[i]);
      switching = true;
      switchcount++;
    } else {
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

sortListDir()

$(".open-modal").on("click", function () {
  $('.category__lvl_3 .siderbar__filter__block[data-modal="' + $(this).data('modal') + '"]').addClass("active");
  return false;
});

$(".open-modal").on("click", function () {
  $('[data-modal="' + $(this).data('modal') + '"]').addClass("active");
  return false;

});

$(".close-modal").on("click", function () {
  $('[data-modal="' + $(this).data('modal') + '"]').removeClass("active");
  return false;
});




var block_show = null;

function scrollTracking() {
  var wt = $(window).scrollTop();
  var wh = $(window).height();
  var et = $('.header').offset().top;
  var eh = $('.header').outerHeight();

  if (wt + wh >= et && wt + wh - eh * 2 <= et + (wh - eh)) {
    if (block_show == null || block_show == false) {
      $('.fix_header').removeClass('active')
    }
    block_show = true;
  } else {
    if (block_show == null || block_show == true) {
      $('.fix_header').addClass('active')
    }
    block_show = false;
  }
}

$(window).scroll(function () {
  scrollTracking();
});

$(document).ready(function () {
  scrollTracking();
});

$('.city_block_item').on('click', function () {
  $('.city_popup').removeClass('active')
  let cityClick = $(this).text()
  let cityHeader = document.querySelector('.header .city__name')
  let cityMenu = document.querySelector('.mobile__nav__block .bottom__block .zag')
  let cityOrder = document.querySelector('.delivery__block .city')
  cityHeader.innerText = cityClick;
  cityMenu.innerText = cityClick;
  cityOrder.innerText = 'г.' + cityClick;
})

$(document).mouseup(function (e) {
  var container = $(".city_popup");
  if (container.has(e.target).length === 0) {
    container.removeClass('active');
  }
});

$('.btn__item.cat_one').on('click', function () {
  $('.stock_block .hostel__wrapper').removeClass('active')
  $('.stock_block .hostel__wrapper.cat_one').addClass('active')
})

$('.btn__item.cat_two').on('click', function () {
  $('.stock_block .hostel__wrapper').removeClass('active')
  $('.hostel__wrapper.cat_two').addClass('active')
})

$('.btn__item.cat_three').on('click', function () {
  $('.stock_block .hostel__wrapper').removeClass('active')
  $('.hostel__wrapper.cat_three').addClass('active')
})

$(document).mouseup(function (e) {
  var container = $(".popup_big_rewiws");
  if (container.has(e.target).length === 0) {
    container.removeClass('active');
  }
});

var swiper_order_cart = new Swiper('.swiper_order_cart', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 5,
  spaceBetween: 10,
  freeMode: false,
  watchSlidesProgress: true,
  slideToClickedSlide: false,
  navigation: {
    nextEl: '.btn_order_cart_next',
    prevEl: '.btn_order_cart_prev',
  },
  breakpoints: {
    1: {
      slidesPerView: 'auto',
      spaceBetween: 9
    },
    450: {
      slidesPerView: 3,
      spaceBetween: 10
    },
    550: {
      slidesPerView: 4,
      spaceBetween: 10
    },
    650: {
      slidesPerView: 5,
      spaceBetween: 10
    },
    992: {
      slidesPerView: 4,
      spaceBetween: 10
    },
    1200: {
      slidesPerView: 5,
      spaceBetween: 10
    }
  }
});

var swiper_order_cart_un = new Swiper('.swiper_order_cart_un', {
  direction: 'horizontal',
  loop: false,
  slidesPerView: 5,
  spaceBetween: 10,
  freeMode: false,
  watchSlidesProgress: true,
  slideToClickedSlide: false,
  navigation: {
    nextEl: '.btn_order_cart_un_next',
    prevEl: '.btn_order_cart_un_prev',
  },
  breakpoints: {
    1: {
      slidesPerView: 'auto',
      spaceBetween: 9
    },
    450: {
      slidesPerView: 3,
      spaceBetween: 10
    },
    550: {
      slidesPerView: 4,
      spaceBetween: 10
    },
    650: {
      slidesPerView: 5,
      spaceBetween: 10
    },
    992: {
      slidesPerView: 4,
      spaceBetween: 10
    },
    1200: {
      slidesPerView: 5,
      spaceBetween: 10
    }
  }
});

function checkSlide() {
  let slideCount = $('.swiper_order_cart_un .swiper-wrapper').children()
  if (slideCount.length <= 5) {
    $('.btn_order_cart_un_prev, .btn_order_cart_un_next').css('display', 'none')
  } else {
    $('.btn_order_cart_un_prev, .btn_order_cart_un_next').css('display', 'flex')
  }
}

checkSlide()

$('.bonus_radio_item').on('click', function () {
  $('.bonus_radio').find('label').removeClass('active')
  $(this).find('label').addClass('active')
})

$('.pickup__block .list__item label').on('click', function () {
  $('.pickup__block .list__item').removeClass('active')
  $(this).parents('.pickup__block .list__item').addClass('active')
})

// Маска номера

$(document).ready(function () {
  $("input[type='tel']").inputmask({
    "mask": "+7 (999) 999-9999", "placeholder": "_",
    showMaskOnHover: false,
  });
});

// Модалка выбора адреса

$('.adress_popup_wrapper_select_item_btn .remove').on('click', function () {
  $(this).parents('.adress_popup_wrapper_select_item').remove()
  return false;
})

$(".open-modal").on("click", function () {
  $('[data-popup="' + $(this).data('popup') + '"]').addClass('active')
  return false;
});

$(".close-modal").on("click", function () {
  $('[data-popup="' + $(this).data('popup') + '"]').removeClass('active')
  return false;
});

$('.adress_popup').on('click', function (e) {
  var target = $(e.target);
  if ($(target).hasClass("active")) {
    $(target).each(function () {
      $(this).removeClass("active");
    });
    setTimeout(function () {
      $(target).removeClass("active");
    }, 350);
  }
});

$('.time__item').on('click', function () {
  $('.time__item').removeClass('active')
  $(this).addClass('active')
})

$(".open-tab").on("click", function () {
  $('.tab_block').removeClass('open')
  $('[data-tab="' + $(this).data('tab') + '"]').addClass('open')
  return false;
});

$('.delivery__item.pickup').on('click', function () {
  $(this).parents('body').find('.pickup_card .delivery__content .title').text('Картой или наличными')
  $(this).parents('body').find('.deliver__wrapper.standart').addClass('width_class')
})

$('.delivery__item.courier').on('click', function () {
  $(this).parents('body').find('.pickup_card .delivery__content .title').text('Картой курьеру')
  $(this).parents('body').find('.deliver__wrapper.standart').removeClass('width_class')
})

$('.swiper_order_cart .swiper-slide').on('click', function () {
  $('.tooltip_order').removeClass('active')
  $(this).parents('.swiper_order_cart').find('.tooltip_order_main').addClass('active')
  $(this).parents('.swiper_order_cart').find('.tooltip_order_main span').text($(this).find('.tooltip_order').text())
})

$('.tooltip_order_main .close_icon').on('click', function () {
  $('.tooltip_order_main').removeClass('active')
})

$(document).mouseup(function (e) {
  var container = $(".tooltip_order_main");
  if (container.has(e.target).length === 0) {
    container.removeClass('active');
  }
});

$('.swiper_order_cart_item').on('click', function () {
  return false;
})

$('.about__user__input_item input, .express__adres_input input').on("click change input", function () {
  if ($(this).val().length > 0) {
    $(this).parent().addClass('active');
  } else {
    $(this).parent().removeClass('active');
  }
});

// Tooltip у товара в order

function createTooltip() {
  let arr = $('.swiper_order_cart_item').toArray();
  $.each(arr, function () {
    $('<div class="tooltip"><div class="icon_close"><svg width="5" height="5" viewBox="0 0 5 5" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M0.320769 4.48874C0.19873 4.3667 0.19873 4.16883 0.320769 4.0468L4.00362 0.363948C4.12566 0.241909 4.32352 0.241909 4.44556 0.363948C4.5676 0.485987 4.5676 0.683851 4.44556 0.80589L0.762711 4.48874C0.640672 4.61078 0.442808 4.61078 0.320769 4.48874Z" fill="#C7BDC2" /> <path d="M0.468083 0.363948C0.590122 0.241909 0.787986 0.24191 0.910025 0.363948L4.59287 4.0468C4.71491 4.16884 4.71491 4.3667 4.59287 4.48874C4.47083 4.61078 4.27297 4.61078 4.15093 4.48874L0.468083 0.80589C0.346044 0.683851 0.346044 0.485987 0.468083 0.363948Z" fill="#C7BDC2" /></svg></div><span></span></div>', {
    }).appendTo($(this));
    $(this).find('.tooltip span').text($(this).attr('data-order-title'))
  })
}

createTooltip()

$('.swiper_order_cart_item').on('click', function (e) {
  var target = $(e.target);
  if ($(target).parents('.swiper_order_cart_item').hasClass("active") || $(target).hasClass("active")) {
    setTimeout(function () {
      $(target).removeClass("active");
      $('.swiper_order_cart_item').removeClass("active");
      $('.tooltip').removeClass("active");
      $('.tooltip_mobile').removeClass('active')
    }, 0);
  } else {
    $('.swiper_order_cart_item').removeClass("active");
    $('.tooltip').removeClass('active')
    $('.tooltip_mobile').removeClass('active')
  }
});

$('.swiper_order_cart_item').on('click', function () {
  $(this).addClass('active')
  $(this).find('.tooltip').addClass('active')
  let parentBlock = $('.swiper_order_cart')
  let posItem = $(this).offset().left - parentBlock.offset().left

  if (posItem < 130) {
    $(this).find('.tooltip').addClass('pos')
  } else {
    $(this).find('.tooltip').removeClass('pos')
  };
});

// // Tooltip у товара в order mobile

$('.swiper_order_cart_item').on('click', function () {
  $(this).addClass('active')
  $(this).parents('.order_cart_block').find(('.tooltip_mobile')).addClass('active')
  $(this).parents('.order_cart_block').find(('.tooltip_mobile')).find('span').text($(this).attr('data-order-title'))
  let parentBlock = $('.swiper_order_cart')
  let posItem = $(this).offset().left - parentBlock.offset().left

  if (posItem < 50) {
    $('.tooltip_mobile').addClass('position-l')
    $('.tooltip_mobile').removeClass('position-c position-r')

  } else if (posItem > 50 && posItem < 149) {
    $('.tooltip_mobile').removeClass('position-l position-r')
    $('.tooltip_mobile').addClass('position-c')

  } else if (posItem > 150) {
    $('.tooltip_mobile').removeClass('position-l position-c')
    $('.tooltip_mobile').addClass('position-r')
  }
});

function orderResize() {
  if ($(window).width() < 450) {
    swiper_order_cart.on('touchMove', function () {
      $('.swiper_order_cart_item').removeClass('active')
      $('.tooltip_mobile').removeClass('active')
      $('.tooltip').removeClass('active')
    })
    swiper_order_cart_un.on('touchMove', function () {
      $('.swiper_order_cart_item').removeClass('active')
      $('.tooltip_mobile').removeClass('active')
      $('.tooltip').removeClass('active')
    })
  }
}

orderResize()

$('.tooltip_mobile .icon_close').on('click', function () {
  $('.tooltip_mobile').removeClass('active')
  $('.swiper_order_cart_item').removeClass('active')
})

$(document).mouseup(function (e) {
  var container = $(".swiper_order_cart_item");
  if (container.has(e.target).length === 0) {
    container.removeClass('active');
    container.find('.tooltip').removeClass('active');
    container.parents('.order_cart_block').find('.tooltip_mobile').removeClass('active');
  }
});


/*
$('.reviews__page .rewiew__item').on('click', function () {
  $('.popup_big_rewiws .user__data .name').text($(this).find('.user__data .name').text())
  $('.popup_big_rewiws .user__data .date').text($(this).find('.user__data .date').text())
  $(this).find('.block__content .text').children().clone().appendTo('.popup_big_rewiws .block__content .text')
})

*/

// Cart page

$('.cart_warning .icon').click(function (e) {
  $(this).parent().fadeOut(400)
  e.preventDefault();

});

$('.value .plus').on('click', function () {
  count = $(this).parents('.value').find('input').val()
  if (count > 0) {
    $(this).parents('.value').find('input').val(++count)
  }
  if (count < 0) {
    count = 1
    $(this).parents('.value').find('input').val(count)
  }
  return false
})

$('.value .minus').on('click', function () {
  count = $(this).parents('.value').find('input').val()
  if (count > 1) {
    $(this).parents('.value').find('input').val(--count)
  }
  if (count < 0) {
    count = 1
    $(this).parents('.value').find('input').val(count)
  }
  return false
})

$('.promocode__block input').on('input', function () {
  if ($(this).val().length > 2) {
    $('.btn__promo').addClass('active')
  } else {
    $('.btn__promo').removeClass('active')
  }
})

// Сколько корзин выбрано
let arrCheckedCart = []

$('.busket_item_haed_check label').click(function (e) {
  if ($(this).prev().prop('checked') === false) {
    $(this).prev().prop('checked', true)
    arrCheckedCart.push($(this))
    e.preventDefault();
    lengthArr = arrCheckedCart.length
    $('.total__item h5 span').text(lengthArr)
  } else {
    $(this).prev().removeAttr('checked')
    $(this).prev().prop('checked', false)
    arrCheckedCart.splice(arrCheckedCart.indexOf($(this)));
    lengthArr = arrCheckedCart.length
    $('.total__item h5 span').text(lengthArr)
    e.preventDefault();
  }
  if (lengthArr === 0) {
    $('.total__item h5').text('Вы не выбрали заказы для оформления')
  } else if (lengthArr === 1) {
    $('.total__item h5').text('Вы выбрали ' + lengthArr + " заказ для оформления")
  } else {
    $('.total__item h5').text('Вы выбрали ' + lengthArr + " заказа для оформления")
  }
});

function cartCheckItem() {
  let arr = $('.order_wrapper input[type=checkbox]').toArray()
  $.each(arr, function () {
    if ($(this).is(':checked')) {
      arrCheckedCart.push($(this))
    }
    lengthArr = arrCheckedCart.length
    $('.total__item h5 span').text(lengthArr)
    if (lengthArr === 0) {
      $('.total__item h5').text('Вы не выбрали заказы для оформления')
    } else if (lengthArr === 1) {
      $('.total__item h5').text('Вы выбрали ' + lengthArr + " заказ для оформления")
    } else {
      $('.total__item h5').text('Вы выбрали ' + lengthArr + " заказа для оформления")
    }
  })
}

cartCheckItem()

$('.item_icon .remove').click(function (e) {
  $(this).parents('.item').remove()
  e.preventDefault();
});

$('.btn_more_text').on('click', function () {
  $(this).toggleClass('active')
  if ($(this).hasClass('active')) {
    $(this).find('span').text('Скрыть')
    $(this).parents('.about_brand_block').find('.about_brand_block_content p').next('p').fadeIn(500)
  } else {
    $(this).find('span').text('Читать подробнее')
    $(this).parents('.about_brand_block').find('.about_brand_block_content p').next('p').fadeOut(300, 0)
  }
  return false;
})

let swiper_arc_brand = new Swiper('.swiper_arc_brand', {
  // Optional parameters
  direction: 'horizontal',
  loop: false,
  slidesPerView: 'auto',
  watchSlidesProgress: true,
  updateOnWindowResize: true,
  // Navigation arrows
  navigation: {
    nextEl: '.arc_btn_next',
    prevEl: '.arc_btn_prev',
  },

});

$('.swiper_arc_brand_item').on('click', function (e) {
  $('.swiper_arc_brand_item.all').removeClass('active')
  $(this).toggleClass('active')
  return false;
});

$('.swiper_arc_brand_item.all').on('click', function (e) {
  $('.swiper_arc_brand_item').removeClass('active')
  $(this).toggleClass('active')
  return false;
});
